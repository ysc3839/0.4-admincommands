#ifdef WIN32
#define WIN32_LEANANDMEAN
#include <Windows.h>

#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#include "plugin.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <process.h>

#define COLOR_BLUE 0x4BAAFFFF
#define COLOR_GRAY 0xFFB0B0B0

#define TEXTDRAW_POS_X 30
#define TEXTDRAW_POS_Y -300
#define TEXTDRAW_LINE_GAP 15

// Global variables (meh)
PluginFuncs * functions;
float TicksElapsed[2];
char AdminPassword[127];
bool ShowServerInfo;
int TextDraws[6];

inline void ZeroBuffer(char * buffer, size_t size) { memset(buffer, 0, size); }
inline void ClientMessage(int nPlayerId, char * pszMsg) { functions->SendClientMessage(nPlayerId, COLOR_BLUE, pszMsg); }
inline bool IsTrue(char * text){ return text == "on" || text == "1" || text == "true" || text == "yes"; }

int OnInitServer();
void OnShutdownServer();
void OnFrame(float fElapsedTime);
int OnCommandMessage(int nPlayerId, const char* pszText);
int GetPlayers();
bool LoadPassword();
bool ParseConfigLine(char * lineBuffer);
void SetAdmin(int nPlayerId, bool isAdmin);
char * GetPlayerName(int nPlayerId);
char * GetPlayerIP(int nPlayerId);
char * GetPlayerUID(int nPlayerId);
void UpdateTextDraw(int i, char * text);
void ShowTextDraw(int ID, bool bShow);
