// Main include
#include "main.h"

// Console stuff
#include "ConsoleUtils.h"

// System info
#include "SystemInfo.h"

#ifdef __cplusplus
extern "C"
#endif
EXPORT unsigned int VcmpPluginInit(PluginFuncs* pluginFuncs, PluginCallbacks* pluginCalls, PluginInfo* pluginInfo)
{
	// Set our plugin information
	pluginInfo->uPluginVer = 0x1000; // 1.0.00
	strcpy_s(pluginInfo->szName, "AdminCommands");

	// Store functions for later use
	functions = pluginFuncs;

	// Store callback
	pluginCalls->OnInitServer = OnInitServer;
	pluginCalls->OnShutdownServer = OnShutdownServer;
	pluginCalls->OnFrame = OnFrame;
	pluginCalls->OnCommandMessage = OnCommandMessage;
	// Done!
	return 1;
}

int OnInitServer()
{
	SysInfoInitialize();
	TextDraws[0] = functions->CreateTextdraw(-1, "--- Server Info ---", TEXTDRAW_POS_X, TEXTDRAW_POS_Y, COLOR_GRAY, false);
	if (LoadPassword())
	{
		char msg[256];
		sprintf_s(msg, "AdminCommands successful loaded! Author:ysc3839 Password:%s", AdminPassword);
		OutputMessage(msg);
	}
	return 1;
}

void OnShutdownServer()
{
	for (int i = 0; i < 6; i++)
	{
		if (TextDraws[i] != 0)
			functions->DestroyTextdraw(TextDraws[i]);
	}
}

void OnFrame(float fElapsedTime)
{
	char szTitle[64];
	TicksElapsed[0] += fElapsedTime * 1000;
	TicksElapsed[1] = TicksElapsed[0];
	if (TicksElapsed[0] >= 1000)
	{
		sprintf_s(szTitle, "Vice City Multiplayer Server :: %u / %u players :: %.0f fps", GetPlayers(), functions->GetMaxPlayers(), 1 / fElapsedTime);
		SetConsoleTitle(szTitle);
		TicksElapsed[0] = 0;
	}
	if (TicksElapsed[1] >= 1000)
	{
		if (ShowServerInfo)
		{
			ShowTextDraw(0, true);

			MEMORYSTATUSEX MemStatus;
			MemStatus.dwLength = sizeof(MemStatus);
			GlobalMemoryStatusEx(&MemStatus);

			char text[64];
			sprintf_s(text, "CPU:%.1f%% Memory:%u%%", GetCpuUsage(), MemStatus.dwMemoryLoad);
			UpdateTextDraw(1, text);

			sprintf_s(text, "Phys Total:%.2fM Free:%.2fM", MemStatus.ullTotalPhys / 1048576.0, MemStatus.ullAvailPhys / 1048576.0);
			UpdateTextDraw(2, text);

			sprintf_s(text, "Page Total:%.2fM Free:%.2fM", MemStatus.ullTotalPageFile / 1048576.0, MemStatus.ullAvailPageFile / 1048576.0);
			UpdateTextDraw(3, text);

			sprintf_s(text, "Virtual Total:%.2fM Free:%.2fM", MemStatus.ullTotalVirtual / 1048576.0, MemStatus.ullAvailVirtual / 1048576.0);
			UpdateTextDraw(4, text);

			int day, hour, minute, second;
			second = GetSystemUptime();
			minute = second / 60;
			second %= 60;
			hour = minute / 60;
			minute %= 60;
			day = hour / 24;
			hour %= 24;
			sprintf_s(text, "Process Count:%u Uptime:%.2u:%.2u:%.2u:%.2u", GetProcessesCount(), day, hour, minute, second);
			UpdateTextDraw(5, text);
		}
		else
		{
			ShowTextDraw(0, false);
			for (int i = 1; i < 6; i++)
			{
				if (TextDraws[i] != 0)
					functions->DestroyTextdraw(TextDraws[i]);
				TextDraws[i] = 0;
			}
		}
		TicksElapsed[1] = 0;
	}
}

int OnCommandMessage(int nPlayerId, const char* pszText)
{
	char * args[8];
	char * split;
	int len = 0;
	split = strtok(const_cast<char*>(pszText), " ");
	while (split != NULL && len < 8)
	{
		args[len] = split;
		split = strtok(NULL, " ");
		len++;
	}
	if (len > 1 && strcmp(strlwr(args[0]), "admin") == 0)
	{
		char * command = strlwr(args[1]);
		if (strcmp(command, "login") == 0)
		{
			if (len >= 3)
			{
				if (strcmp(args[2], AdminPassword) == 0)
				{
					SetAdmin(nPlayerId, true);
					ClientMessage(nPlayerId, "*> Logged in as admin. <*");
				}
				else
				{
					char msg[256];
					sprintf_s(msg, "AdminCommands: Player '%s' ID %d failed to login admin! Wrong password:%s", GetPlayerName(nPlayerId), nPlayerId, args[2]);
					OutputWarning(msg);
					ClientMessage(nPlayerId, "*> Failed to login in admin. <*");
				}
			}
		}
		else if (functions->IsPlayerAdmin(nPlayerId))
		{
			if (strcmp(command, "clientmessage") == 0)
			{
				if (len >= 4)
				{
					ClientMessage(atoi(args[2]), args[3]);
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin clientmessage <PlayerID> <Msg>");
				}
			}
			else if (strcmp(command, "gamemessage") == 0)
			{
				if (len >= 5)
				{
					functions->SendGameMessage(atoi(args[2]), atoi(args[3]), args[4]);
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin gamemessage <PlayerID> <Type> <Msg>");
				}
			}
			else if (strcmp(command, "maxplayers") == 0)
			{
				if (len >= 3)
				{
					functions->SetMaxPlayers(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin maxplayers <Max>");
				}
			}
			else if (strcmp(command, "settimerate") == 0)
			{
				if (len >= 3)
				{
					functions->SetTimeRate(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin settimerate <TimeRate>");
				}
			}
			else if (strcmp(command, "sethr") == 0)
			{
				if (len >= 3)
				{
					functions->SetHour(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin sethr <Hour>");
				}
			}
			else if (strcmp(command, "setmin") == 0)
			{
				if (len >= 3)
				{
					functions->SetMinute(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setmin <Minute>");
				}
			}
			else if (strcmp(command, "setweather") == 0)
			{
				if (len >= 3)
				{
					functions->SetWeather(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setweather <Weather>");
				}
			}
			else if (strcmp(command, "setgravity") == 0)
			{
				if (len >= 3)
				{
					functions->SetGravity(atof(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setgravity <Gravity>");
				}
			}
			else if (strcmp(command, "setgamespeed") == 0)
			{
				if (len >= 3)
				{
					functions->SetGamespeed(atof(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setgamespeed <Gamespeed>");
				}
			}
			else if (strcmp(command, "setwaterlevel") == 0)
			{
				if (len >= 3)
				{
					functions->SetWaterLevel(atof(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setwaterlevel <WaterLevel>");
				}
			}
			else if (strcmp(command, "setmaxheight") == 0)
			{
				if (len >= 3)
				{
					functions->SetMaxHeight(atof(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setmaxheight <MaxHeight>");
				}
			}
			else if (strcmp(command, "setkillcmddelay") == 0)
			{
				if (len >= 3)
				{
					functions->SetKillCmdDelay(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setkillcmddelay <KillCmdDelay>");
				}
			}
			else if (strcmp(command, "setvehiclesforcedrespawnheight") == 0)
			{
				if (len >= 3)
				{
					functions->SetVehiclesForcedRespawnHeight(atof(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setvehiclesforcedrespawnheight <VehiclesForcedRespawnHeight>");
				}
			}
			else if (strcmp(command, "setadmin") == 0)
			{
				if (len >= 4)
				{
					SetAdmin(atoi(args[2]), IsTrue(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setadmin <PlayerID> <IsAdmin>");
				}
			}
			else if (strcmp(command, "setplrworld") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerWorld(atoi(args[2]), atoi(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setplrworld <PlayerID> <WorldID>");
				}
			}
			else if (strcmp(command, "setplrname") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerName(atoi(args[2]), args[3]);
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setplrname <PlayerID> <Name>");
				}
			}
			else if (strcmp(command, "setplrteam") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerTeam(atoi(args[2]), atoi(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setplrteam <PlayerID> <TeamID>");
				}
			}
			else if (strcmp(command, "setplrskin") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerSkin(atoi(args[2]), atoi(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setplrskin <PlayerID> <SkinID>");
				}
			}
			else if (strcmp(command, "setplrcolor") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerColour(atoi(args[2]), atoi(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setplrcolor <PlayerID> <Color>");
				}
			}
			else if (strcmp(command, "forceplrspawn") == 0)
			{
				if (len >= 3)
				{
					functions->ForcePlayerSpawn(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin forceplrspawn <PlayerID>");
				}
			}
			else if (strcmp(command, "setplrmoney") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerMoney(atoi(args[2]), atoi(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setplrmoney <PlayerID> <Money>");
				}
			}
			else if (strcmp(command, "setplrscore") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerScore(atoi(args[2]), atoi(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setplrscore <PlayerID> <Score>");
				}
			}
			else if (strcmp(command, "sethp") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerHealth(atoi(args[2]), atof(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin sethp <PlayerID> <Health>");
				}
			}
			else if (strcmp(command, "setarmour") == 0)
			{
				if (len >= 4)
				{
					functions->SetPlayerArmour(atoi(args[2]), atof(args[3]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setarmour <PlayerID> <Armour>");
				}
			}
			else if (strcmp(command, "setalpha") == 0)
			{
				if (len >= 5)
				{
					functions->SetPlayerAlpha(atoi(args[2]), atoi(args[3]), atoi(args[4]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin setalpha <PlayerID> <Alpha> <FadeTime>");
				}
			}
			else if (strcmp(command, "kick") == 0)
			{
				if (len >= 3)
				{
					functions->KickPlayer(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin kick <PlayerID>");
				}
			}
			else if (strcmp(command, "ban") == 0)
			{
				if (len >= 3)
				{
					functions->BanPlayer(atoi(args[2]));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin ban <PlayerID>");
				}
			}
			else if (strcmp(command, "banip") == 0)
			{
				if (len >= 3)
				{
					functions->BanIP(args[2]);
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin banip <IP>");
				}
			}
			else if (strcmp(command, "unban") == 0)
			{
				if (len >= 3)
				{
					functions->UnbanIP(args[2]);
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin unban <IP>");
				}
			}
			else if (strcmp(command, "ip") == 0)
			{
				if (len >= 3)
				{
					ClientMessage(nPlayerId, GetPlayerIP(atoi(args[2])));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin ip <PlayerID>");
				}
			}
			else if (strcmp(command, "fps") == 0)
			{
				if (len >= 3)
				{
					functions->SendClientMessage(nPlayerId, COLOR_BLUE, "%f", functions->GetPlayerFPS(atoi(args[2])));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin fps <PlayerID>");
				}
			}
			else if (strcmp(command, "uid") == 0)
			{
				if (len >= 3)
				{
					ClientMessage(nPlayerId, GetPlayerUID(atoi(args[2])));
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin uid <PlayerID>");
				}
			}
			else if (strcmp(command, "goto") == 0)
			{
				if (len >= 3)
				{
					float x, y, z;
					functions->GetPlayerPos(atoi(args[2]), &x, &y, &z);
					functions->SetPlayerPos(nPlayerId, x, y, z);
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin goto <PlayerID>");
				}
			}
			else if (strcmp(command, "bring") == 0)
			{
				if (len >= 3)
				{
					float x, y, z;
					functions->GetPlayerPos(nPlayerId, &x, &y, &z);
					functions->SetPlayerPos(atoi(args[2]), x, y, z);
				}
				else
				{
					ClientMessage(nPlayerId, "Usage: /admin bring <PlayerID>");
				}
			}
			else if (strcmp(command, "exitserver") == 0)// There may be something wrong...
			{
				//GenerateConsoleCtrlEvent(0, 0);
				functions->ShutdownServer();
			}
			else if (strcmp(command, "serverinfo") == 0)
			{
				ShowServerInfo = !ShowServerInfo;
				functions->SendClientMessage(nPlayerId, COLOR_BLUE, "ServerInfo %s", ShowServerInfo ? "Enabled" : "Disabled");
			}
			else
			{
				ClientMessage(nPlayerId, "I don't know that command.");
			}
		}
	}

	return 1;
}

int GetPlayers()
{
	int count = 0;
	for (int i = 0; i < functions->GetMaxPlayers(); i++)
		if (functions->IsPlayerConnected(i))
			count++;

	return count;
}

bool LoadPassword() //From 0.4 Squirrel
{
	FILE * file;
	bool PasswordFound = false;

	file = fopen("server.cfg", "r");
	if (file == NULL)
		OutputError("AdminCommands could not read server.cfg");
	else
	{
		// Is this clean?
		int lineSize = 128;
		int elapsedLineSize = 0;
		char * lineBuffer = (char *)malloc(sizeof(char) * lineSize);

		// Did we do it?
		if (lineBuffer == NULL)
			OutputError("AdminCommands could not allocate memory to read server.cfg");
		else
		{
			ZeroBuffer(lineBuffer, lineSize);
			char ch = getc(file);

			while (ch != EOF)
			{
				if (ch == '\n')
				{
					// End of the line. Parse it.
					if (ParseConfigLine(lineBuffer))
					{
						PasswordFound = true;
						break;
					}

					ZeroBuffer(lineBuffer, lineSize);
					elapsedLineSize = 0;
				}
				else
				{
					lineBuffer[elapsedLineSize++] = ch;

					// If we've hit our limit on line size, stop reading the line.
					if (elapsedLineSize + 1 == lineSize)
					{
						// Parse it.
						if (ParseConfigLine(lineBuffer))
						{
							PasswordFound = true;
							break;
						}

						// Go straight to the next one instead.
						while (ch != '\n')
							ch = getc(file);

						ZeroBuffer(lineBuffer, lineSize);
						elapsedLineSize = 0;
					}
				}

				// Get the next character in the file.
				ch = getc(file);
			}

			if (!PasswordFound && ParseConfigLine(lineBuffer))
				PasswordFound = true;
		}

		// Clean up our shit
		free(lineBuffer);
	}
	// More cleaning
	fclose(file);

	if (!PasswordFound)
		OutputError("No password was specified.");

	return PasswordFound;
}

bool ParseConfigLine(char * lineBuffer) //From 0.4 Squirrel
{
	char * PasswordSearch = NULL;
	if ((PasswordSearch = strstr(lineBuffer, "adminpass ")) == NULL)
		return false;
	else if (strlen(PasswordSearch) < 1)
		return false;
	else
	{
		// Ew.
		PasswordSearch += sizeof("adminpass");
		strcpy_s(AdminPassword, PasswordSearch);

		return true;
	}
}

void SetAdmin(int nPlayerId, bool isAdmin)
{
	char msg[128];
	functions->SetPlayerAdmin(nPlayerId, isAdmin);
	sprintf_s(msg, "AdminCommands: Set player '%s' ID %d %s admin!", GetPlayerName(nPlayerId), nPlayerId, isAdmin ? "AS" : "NOT");
	OutputMessage(msg);
}

char * GetPlayerName(int nPlayerId)
{
	static char name[65];
	ZeroBuffer(name, 65);
	functions->GetPlayerName(nPlayerId, name, 64);
	return name;
}

char * GetPlayerIP(int nPlayerId)
{
	static char IP[17];
	ZeroBuffer(IP, 17);
	functions->GetPlayerIP(nPlayerId, IP, 16);
	return IP;
}

char * GetPlayerUID(int nPlayerId)
{
	static char UID[41];
	ZeroBuffer(UID, 41);
	functions->GetPlayerUID(nPlayerId, UID, 40);
	return UID;
}

void UpdateTextDraw(int i, char * text)
{
	if (TextDraws[i] != 0)
		functions->DestroyTextdraw(TextDraws[i]);
	TextDraws[i] = functions->CreateTextdraw(-1, text, TEXTDRAW_POS_X, TEXTDRAW_POS_Y + i * TEXTDRAW_LINE_GAP, COLOR_GRAY, false);
	ShowTextDraw(i, true);
}

void ShowTextDraw(int ID, bool bShow)
{
	if (bShow)
	{
		for (int i = 0; i < functions->GetMaxPlayers(); i++)
		{
			if (functions->IsPlayerAdmin(i))
				functions->ShowTextdraw(TextDraws[ID], i);
		}
	}
	else
		functions->HideTextdraw(TextDraws[ID], -1);
}
