#include "SystemInfo.h"
#include "ConsoleUtils.h"

#ifdef USE_NTDLL
#pragma comment(lib, "ntdll.lib")

#define Li2Double(x) ((double)((x).HighPart) * 4.294967296E9 + (double)((x).LowPart))

typedef enum _SYSTEM_INFORMATION_CLASS {
	SystemBasicInformation = 0,
	SystemPerformanceInformation = 2,
	SystemTimeOfDayInformation = 3,
	SystemProcessInformation = 5,
	SystemProcessorPerformanceInformation = 8,
	SystemInterruptInformation = 23,
	SystemExceptionInformation = 33,
	SystemRegistryQuotaInformation = 37,
	SystemLookasideInformation = 45,
	SystemPolicyInformation = 134,
} SYSTEM_INFORMATION_CLASS;

typedef struct
{
	LARGE_INTEGER liIdleTime;
	ULONG dwSpare[76];
} SYSTEM_PERFORMANCE_INFORMATION;

typedef struct
{
	DWORD dwUnknown1;
	ULONG uKeMaximumIncrement;
	ULONG uPageSize;
	ULONG uMmNumberOfPhysicalPages;
	ULONG uMmLowestPhysicalPage;
	ULONG uMmHighestPhysicalPage;
	ULONG uAllocationGranularity;
	PVOID pLowestUserAddress;
	PVOID pMmHighestUserAddress;
	ULONG uKeActiveProcessors;
	BYTE bKeNumberProcessors;
	BYTE bUnknown2;
	WORD wUnknown3;
} SYSTEM_BASIC_INFORMATION;

typedef struct
{
	LARGE_INTEGER liKeBootTime;
	LARGE_INTEGER liKeSystemTime;
	LARGE_INTEGER liExpTimeZoneBias;
	ULONG uCurrentTimeZoneId;
	DWORD dwReserved;
} SYSTEM_TIME_INFORMATION;

extern "C"
NTSYSAPI
NTSTATUS
NTAPI
NtQuerySystemInformation(IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
OUT PVOID SystemInformation,
IN ULONG SystemInformationLength,
OUT PULONG ReturnLength OPTIONAL);

SYSTEM_BASIC_INFORMATION SysBaseInfo;
#else
#define FileTimeToDouble(x) ((double)((x).dwHighDateTime) * 4.294967296E9 + (double)((x).dwLowDateTime))

double fOldCPUIdleTime, fOldCPUKernelTime, fOldCPUUserTime;
#endif

void SysInfoInitialize()
{
#ifdef USE_NTDLL
	if (NtQuerySystemInformation(SystemBasicInformation, &SysBaseInfo, sizeof(SysBaseInfo), NULL) != 0)
		OutputError("NtQuerySystemInformation Error!");
#endif
}

double GetCpuUsage()
{
#ifdef USE_NTDLL
	SYSTEM_PERFORMANCE_INFORMATION SysPerfInfo;
	SYSTEM_TIME_INFORMATION SysTimeInfo;
	double dbIdleTime = 0;
	double dbSystemTime = 0;
	NTSTATUS status;
	static LARGE_INTEGER liOldIdleTime;
	static LARGE_INTEGER liOldSystemTime;

	status = NtQuerySystemInformation(SystemTimeOfDayInformation, &SysTimeInfo, sizeof(SysTimeInfo), NULL);
	if (status == 0)
	{
		status = NtQuerySystemInformation(SystemPerformanceInformation, &SysPerfInfo, sizeof(SysPerfInfo), NULL);
		if (status == 0)
		{
			if (liOldIdleTime.QuadPart != 0)
			{
				dbIdleTime = Li2Double(SysPerfInfo.liIdleTime) - Li2Double(liOldIdleTime);
				dbSystemTime = Li2Double(SysTimeInfo.liKeSystemTime) - Li2Double(liOldSystemTime);

				dbIdleTime = dbIdleTime / dbSystemTime;

				dbIdleTime = 100.0 - dbIdleTime * 100.0 / (double)SysBaseInfo.bKeNumberProcessors + 0.5;
			}
			liOldIdleTime = SysPerfInfo.liIdleTime;
			liOldSystemTime = SysTimeInfo.liKeSystemTime;
		}
	}
	return dbIdleTime;
#else
	double dbCPUUseRate = 0;
	FILETIME ftIdle, ftKernel, ftUser;
	double fCPUIdleTime = 0;
	double fCPUKernelTime = 0;
	double fCPUUserTime = 0;
	if (GetSystemTimes(&ftIdle, &ftKernel, &ftUser))
	{
		fCPUIdleTime = FileTimeToDouble(ftIdle);
		fCPUKernelTime = FileTimeToDouble(ftKernel);
		fCPUUserTime = FileTimeToDouble(ftUser);
		if (fOldCPUIdleTime != 0)
		{
			dbCPUUseRate = (100.0 - (fCPUIdleTime - fOldCPUIdleTime) / (fCPUKernelTime - fOldCPUKernelTime + fCPUUserTime - fOldCPUUserTime) * 100.0);
		}
		fOldCPUIdleTime = fCPUIdleTime;
		fOldCPUKernelTime = fCPUKernelTime;
		fOldCPUUserTime = fCPUUserTime;
	}
	return dbCPUUseRate;
#endif
}

int GetProcessesCount()
{
	DWORD aProcesses[1024], cbNeeded, cProcesses;
	if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
		return 0;
	cProcesses = cbNeeded / sizeof(DWORD);
	return cProcesses;
}

int GetSystemUptime()
{
#ifdef USE_NTDLL
	SYSTEM_TIME_INFORMATION SysTimeInfo;
	NTSTATUS status = NtQuerySystemInformation(SystemTimeOfDayInformation, &SysTimeInfo, sizeof(SysTimeInfo), NULL);
	if (status == 0)
		return (SysTimeInfo.liKeSystemTime.QuadPart - SysTimeInfo.liKeBootTime.QuadPart) / 10000000;
	return 0;
#else
	return GetTickCount();
#endif
}
