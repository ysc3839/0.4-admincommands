#ifdef WIN32
#define WIN32_LEANANDMEAN
#include <Windows.h>
#include <Psapi.h>
//#define USE_NTDLL 0
#endif

void SysInfoInitialize();
double GetCpuUsage();
int GetProcessesCount();
int GetSystemUptime();
