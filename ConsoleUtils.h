#ifdef WIN32
	#define WIN32_LEANANDMEAN
	#include <Windows.h>
#endif
#include <stdio.h>

void OutputDebug(const char * msg);
void OutputMessage(const char * msg);
void OutputWarning(const char * msg);
void OutputError(const char * msg);
